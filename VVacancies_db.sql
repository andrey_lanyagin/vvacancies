-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE DATABASE "VVacancies" ----------------------------
CREATE DATABASE IF NOT EXISTS `VVacancies` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `VVacancies`;
-- ---------------------------------------------------------


-- CREATE TABLE "departments" ------------------------------
DROP TABLE IF EXISTS `departments` CASCADE;

CREATE TABLE `departments` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`title` VarChar( 255 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 8;
-- ---------------------------------------------------------


-- CREATE TABLE "dictionary_languages" ---------------------
DROP TABLE IF EXISTS `dictionary_languages` CASCADE;

CREATE TABLE `dictionary_languages` ( 
	`code` VarChar( 255 ) NOT NULL,
	`language` VarChar( 255 ) NOT NULL,
	PRIMARY KEY ( `code` ) )
ENGINE = InnoDB;
-- ---------------------------------------------------------


-- CREATE TABLE "vacancies" --------------------------------
DROP TABLE IF EXISTS `vacancies` CASCADE;

CREATE TABLE `vacancies` ( 
	`id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`department_id` Int( 11 ) UNSIGNED NOT NULL,
	`title` VarChar( 255 ) NOT NULL,
	`description` VarChar( 255 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 10;
-- ---------------------------------------------------------


-- CREATE TABLE "vacant_translations" ----------------------
DROP TABLE IF EXISTS `vacant_translations` CASCADE;

CREATE TABLE `vacant_translations` ( 
	`vacant_id` Int( 255 ) UNSIGNED NOT NULL,
	`language_code` VarChar( 255 ) NOT NULL,
	`vacant_name` VarChar( 255 ) NULL,
	`vacant_description` VarChar( 255 ) NULL )
ENGINE = InnoDB;
-- ---------------------------------------------------------


-- Dump data of "departments" ------------------------------

INSERT INTO `departments`(`id`,`title`) VALUES ( '2', 'Accounting and Finance' );
INSERT INTO `departments`(`id`,`title`) VALUES ( '3', 'Art, Design and Printing' );
INSERT INTO `departments`(`id`,`title`) VALUES ( '4', 'Buildings Maintenance' );
INSERT INTO `departments`(`id`,`title`) VALUES ( '5', 'Communications' );
INSERT INTO `departments`(`id`,`title`) VALUES ( '6', 'Finance' );
INSERT INTO `departments`(`id`,`title`) VALUES ( '7', 'President Office' );

-- ---------------------------------------------------------


-- Dump data of "dictionary_languages" ---------------------

INSERT INTO `dictionary_languages`(`code`,`language`) VALUES ( 'eng', 'English' );
INSERT INTO `dictionary_languages`(`code`,`language`) VALUES ( 'est', 'Estonian' );
INSERT INTO `dictionary_languages`(`code`,`language`) VALUES ( 'fin', 'Finnish' );
INSERT INTO `dictionary_languages`(`code`,`language`) VALUES ( 'fr', 'France' );
INSERT INTO `dictionary_languages`(`code`,`language`) VALUES ( 'ger', 'German' );
INSERT INTO `dictionary_languages`(`code`,`language`) VALUES ( 'it', 'Italian' );
INSERT INTO `dictionary_languages`(`code`,`language`) VALUES ( 'ru', 'Russian' );

-- ---------------------------------------------------------


-- Dump data of "vacancies" --------------------------------

INSERT INTO `vacancies`(`id`,`department_id`,`title`,`description`) VALUES ( '2', '2', 'Vacancy 2_1', 'Vacancy 1 department #2' );
INSERT INTO `vacancies`(`id`,`department_id`,`title`,`description`) VALUES ( '3', '2', 'Vacancy 2_2', 'Vacancy 2 department #2' );
INSERT INTO `vacancies`(`id`,`department_id`,`title`,`description`) VALUES ( '4', '2', 'Vacancy 2_3', 'Vacancy 3 department #2' );
INSERT INTO `vacancies`(`id`,`department_id`,`title`,`description`) VALUES ( '5', '2', 'Vacancy 2_4', 'Vacancy 4 department #2' );
INSERT INTO `vacancies`(`id`,`department_id`,`title`,`description`) VALUES ( '6', '3', 'Vacancy 3_1', 'Vacancy 1 department #3' );
INSERT INTO `vacancies`(`id`,`department_id`,`title`,`description`) VALUES ( '7', '3', 'Vacancy 3_2', 'Vacancy 2 department #3' );
INSERT INTO `vacancies`(`id`,`department_id`,`title`,`description`) VALUES ( '8', '3', 'Vacancy 3_3', 'Vacancy 3 department #3' );
INSERT INTO `vacancies`(`id`,`department_id`,`title`,`description`) VALUES ( '9', '3', 'Vacancy 3_4', 'Vacancy 4 department #3' );

-- ---------------------------------------------------------


-- Dump data of "vacant_translations" ----------------------

INSERT INTO `vacant_translations`(`vacant_id`,`language_code`,`vacant_name`,`vacant_description`) VALUES ( '2', 'est', 'Est title', 'Est Desc' );
INSERT INTO `vacant_translations`(`vacant_id`,`language_code`,`vacant_name`,`vacant_description`) VALUES ( '2', 'fr', 'Fr title', 'Fr Desc' );
INSERT INTO `vacant_translations`(`vacant_id`,`language_code`,`vacant_name`,`vacant_description`) VALUES ( '2', 'fin', 'Fin title', 'Fin Desc' );

-- ---------------------------------------------------------


-- CREATE INDEX "index_language" ---------------------------
CREATE INDEX `index_language` USING BTREE ON `dictionary_languages`( `language` );
-- ---------------------------------------------------------


-- CREATE INDEX "VACANT_DEPTS_IDX" -------------------------
CREATE INDEX `VACANT_DEPTS_IDX` USING BTREE ON `vacancies`( `department_id` );
-- ---------------------------------------------------------


-- CREATE INDEX "index_vacant_id" --------------------------
CREATE INDEX `index_vacant_id` USING BTREE ON `vacant_translations`( `vacant_id` );
-- ---------------------------------------------------------


-- CREATE INDEX "vacant_lang_idx" --------------------------
CREATE INDEX `vacant_lang_idx` USING BTREE ON `vacant_translations`( `language_code` );
-- ---------------------------------------------------------


-- CREATE LINK "lnk_departments_vacancies" -----------------
ALTER TABLE `vacancies`
	ADD CONSTRAINT `lnk_departments_vacancies` FOREIGN KEY ( `department_id` )
	REFERENCES `departments`( `id` )
	ON DELETE Restrict
	ON UPDATE No Action;
-- ---------------------------------------------------------


-- CREATE LINK "lnk_dictionary_languages_vacant_translations" 
ALTER TABLE `vacant_translations`
	ADD CONSTRAINT `lnk_dictionary_languages_vacant_translations` FOREIGN KEY ( `language_code` )
	REFERENCES `dictionary_languages`( `code` )
	ON DELETE Restrict
	ON UPDATE Cascade;
-- ---------------------------------------------------------


-- CREATE LINK "lnk_vacancies_vacant_translations" ---------
ALTER TABLE `vacant_translations`
	ADD CONSTRAINT `lnk_vacancies_vacant_translations` FOREIGN KEY ( `vacant_id` )
	REFERENCES `vacancies`( `id` )
	ON DELETE Restrict
	ON UPDATE Cascade;
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


