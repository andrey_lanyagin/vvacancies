var DEFAULT_GRID_HEIGHT = $(window).height() - 190;

var departmentComboStore = new Ext.data.JsonStore({
	autoDestroy: true,
	autoload: false,
	url: 'index.php?r=site/departmentlist',
	storeId: 'deptSore',
	root: 'data',
	idProperty: 'id',
	fields: ['id', 'title']
});

var languageComboStore = new Ext.data.JsonStore({
	autoDestroy: true,
	autoload: true,
	url: 'index.php?r=site/languagelist',
	storeId: 'deptSore',
	root: 'data',
	idProperty: 'code',
	fields: ['code', 'language']
});

var departmentBox = new Ext.form.ComboBox({
	store: departmentComboStore,
	id: 'departmentBox',
	name: 'departmentBox',
	typeAhead: true,
	triggerAction: 'all',
	lazyRender: true,
	valueField: 'id',
	displayField: 'title',
	mode: 'local',
	width: 150,
	listeners: {'select': function (combo, record, index) {
			mainStore.setBaseParam('departmentId', record.data.id);
			mainStore.load();
		}
	}
});

var languageBox = new Ext.form.ComboBox({
	store: languageComboStore,
	id: 'languageBox',
	name: 'languageBox',
	typeAhead: true,
	triggerAction: 'all',
	lazyRender: true,
	valueField: 'code',
	displayField: 'language',
	mode: 'local',
	width: 150,
	listeners: {'select': function (combo, record, index) {
			mainStore.setBaseParam('languageCode', record.data.code);
			mainStore.load();
		}}
});

var mainStore = new Ext.data.JsonStore({
	autoDestroy: false,
	autoLoad: false,
	autoSave: false,
	storeId: 'addlAgreementStore',
	root: 'data',
	url: 'index.php?r=site/vacancylist',
	fields: [
		{name: 'department', type: 'string'},
		{name: 'vacancy', type: 'string'},
		{name: 'description', type: 'string'}
	]
});

var mainGrid = new Ext.grid.GridPanel({
	id: 'mainGrid',
	title: 'Available vacancies',
	store: mainStore,
	columnLines: true,
	tbar: ['Department: ', departmentBox, '-', 'Language: ', languageBox],
	autoExpandColumn: 'description',
	columns: [
		{
			id: 'departments',
			header: 'Department',
			width: 100,
			dataIndex: 'department'
		}, {
			id: 'vacancy',
			header: 'Title',
			dataIndex: 'vacancy'
		}, {
			id: 'description',
			header: 'Description',
			width: 100,
			dataIndex: 'description'
		}]
});

Ext.onReady(function () {

	new Ext.Panel({
		layout: 'fit',
		renderTo: 'content',
		height: DEFAULT_GRID_HEIGHT,
		items: [mainGrid]
	});

	languageComboStore.load();
	departmentComboStore.load();
	mainStore.load();
});