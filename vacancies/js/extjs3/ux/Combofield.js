Ext.ns('Ext.ux');

Ext.ux.Combofield = Ext.extend(Ext.form.ComboBox, {
	/**
	 * Инициализирует компонент
	 */

	storeUrl: '/',
	displayField: 'title',
	valueField: 'id',
	typeAhead:true,
	minChars:1,
	forceSelection: true,
	triggerAction: 'all',
	mode: 'local',
	tpl: new Ext.XTemplate(
	        '<tpl for="."><div class="combo-view-item">',
	           '{title}',
	        '</div></tpl>'
	    		),
	itemSelector: 'div.combo-view-item',
		
	initComponent: function(){
		var handler = this;
		handler.hiddenName  = handler.name;
		
		handler.store = new Ext.data.JsonStore({
    		autoLoad: true,
    		root: 'data',
    		url: handler.storeUrl,
    		fields: [
    		         {name: 'id'},
    		         {name: 'title'}
    		],
    		//data: this.dataContainer,
    		listeners:{
		    	'load': function(){
		    		
		    		var value = handler.getValue();
		    		
		    		if(value)
		    			handler.setValue(value);
		    	}
		    }
    	});
		
		Ext.ux.Combofield.superclass.initComponent.call(this);
    }
});

Ext.reg('ux_combofield', Ext.ux.Combofield);
