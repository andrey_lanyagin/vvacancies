/*
 * Выбор значения для ОКАТО
 */
var oktmoStore = new Ext.data.JsonStore({
	autoLoad: true,
	root: 'data',
	fields: [
	         'code',
	         'name'
	],
	url:"/json/oktmo/region/"
});

Ext.ux.Oktmo = Ext.extend( Ext.Panel, {
	
	layout: 'form',
	border: false,
	frame: false,
	
	initComponent: function()
	{
         Ext.apply(this, 
         {
			 defaults:{
				anchor: '100%'
			 },
        	 items: [ 
				    {
				    	xtype: 'combo',
				    	name: 'oktmo_code',
				    	hiddenName: 'oktmo_code',
				    	id: 'oktmo_code',
				    	fieldLabel:"ОКТМО",
				    	displayField: "name",
				    	valueField:"code",
				    	typeAhead: true,
				    	lazyRender: true,
				    	triggerAction:"all",
				    	mode:"local",
				    	store: oktmoStore				    	
				    }
				]
         });
         
         Ext.ux.Oktmo.superclass.initComponent.apply(this, arguments);
	 },
	 
	 addComboPartition: function(){
		 
	 }
	
});

Ext.reg('oktmo', Ext.ux.Oktmo);
