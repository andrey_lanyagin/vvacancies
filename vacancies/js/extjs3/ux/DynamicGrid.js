/*
 * Динамический Grig
 */
Ext.ux.DynamicGrid = Ext.extend(Ext.grid.GridPanel, {
	/**
	 * Инициализирует компонент
	 */

	constructor: function(config){
		Ext.ux.DynamicGrid.superclass.constructor.call(this, config);
		
		if(!this.store)
			this.store = new Ext.data.JsonStore({storeId: 'dynamicStore', id: 'dynamicStore'});
    },

	initComponent: function(){
		
		this.viewConfig = Ext.apply(this.viewConfig || {}, {
            forceFit: false
        });

		this.colModel = new Ext.grid.ColumnModel({});
		this.buildColumn(this.columnUrl, this.storeUrl);	
		
		Ext.ux.DynamicGrid.superclass.initComponent.apply(this, arguments);
	},
	/**
	 * Загрузка калонок компонента
	 */
	 buildColumn: function(columnUrl){
		var handler = this;
		
		Ext.Ajax.request({
	 		url: columnUrl,
	        success: function(response, request) {
	        	response =  Ext.util.JSON.decode(response.responseText);
	      		if(response.success)
	      		{
	      			handler.colModel.setConfig(response.columns);
	      			if(handler.grouping != undefined && handler.grouping)
	      				handler.setStore({
	      					grouping: true,
	      					groupingField: handler.groupingField
	      				});
					else handler.setStore({});
	      		}
	        }
		});
		
	},
	/**
	 * Формирование хранилища
	 */
	 setStore: function(params){	
		var numCols = this.getColumnModel().getColumnCount();
		var fields = new Array();

		for(var i=0; i<numCols; i++)
				fields[fields.length] = this.colModel.getDataIndex(i);

		if(false /* params.grouping != undefined && params.grouping */)
		{
			var store = new Ext.data.GroupingStore({
				id: 'dynamicStore',
				storeId: 'dynamicStore',
				groupField: params.groupingField,
				autoLoad: false,
				url: this.storeUrl,
				root: "data",
				fields: fields
			});
		}
		else
		{
			var store = new Ext.data.JsonStore({
				id: 'dynamicStore',
				storeId: 'dynamicStore',
				autoLoad: false,
				url: this.storeUrl,
				root: "data",
				fields: fields
			});
		}
		//this.rendered = true;
		this.reconfigure(store, this.colModel);
		
		setStoreListeners();
	},
	/**
	 * Дополнить настройки Grid'а
	 */
	reConfig: function(config){
		Ext.apply(this, config);
	}
});

Ext.reg('dynamicgrid', Ext.ux.DynamicGrid);
