/*
 * Выбор значения для ОКАТО
 */
Ext.ux.Okato = Ext.extend( Ext.Panel, {
	
	layout: 'form',
	border: false,
	frame: false,
	
	initComponent: function()
	{
         Ext.apply(this, 
         {
			 defaults:{
				anchor: '100%'
			 },
        	 items: [ 
        	        {
        	        	id: 'okato_code_filed',
        	        	xtype: 'hidden',
        	        	name: 'okato_code'
        	        },
				    {
				    	xtype: 'combo',
				    	id: 'okato_region_combo',
				    	fieldLabel:"Субъект РФ",
				    	displayField: "name",
				    	valueField:"code",
				    	typeAhead: true,
				    	lazyRender: true,
				    	triggerAction:"all",
				    	mode:"local",
				    	store: new Ext.data.JsonStore({
				    		autoLoad: true,
				    		root: 'data',
				    		fields: [
				    		         'code',
				    		         'name'
				    		],
				    		url:"/json/okato/"
				    	}),
				    	listeners : {
				    		select: function(combo, record, index){
				    			/*
				    			 * Сбрасываем данные на случай неудачной загрузки хранилищ
				    			 */
			    				var okato_ron = Ext.getCmp('okato_ron_combo').getStore();
				    			okato_ron.removeAll();
		
				    			okato_ron.setBaseParam('code', record.data.code);
				    			okato_ron.load();
				    			/*
				    			var okato_city = Ext.getCmp('okato_city_combo').getStore();
			    				okato_city.removeAll();
								*/
				    			if(record.data.code)
				    				Ext.getCmp('okato_code_filed').setValue(record.data.code);
				    		}
				    	}
				    },
				    {
				    	xtype: 'combo',
				    	id: 'okato_ron_combo',
				    	fieldLabel:"Муниципальное образование",
				    	displayField: "name",
				    	valueField:"code",
				    	typeAhead: true,
				    	lazyRender: true,
				    	triggerAction:"all",
				    	mode:"local",
				    	store: new Ext.data.JsonStore({
				    		autoLoad: false,
				    		root: 'data',
				    		fields: [
				    		         'code',
				    		         'name'
				    		],
				    		url:"/json/okato/"
				    	}),
				    	listeners : {
				    		select: function(combo, record, index){
				    			if(record.data.code)
				    				Ext.getCmp('okato_code_filed').setValue(record.data.code);
				    		}
				    	}
				    }/*,
				    {
				    	xtype: 'combo',
				    	id: 'okato_city_combo',
				    	fieldLabel:"Раздел 3",
				    	displayField: "name",
				    	valueField:"code",
				    	typeAhead: true,
				    	lazyRender: true,
				    	triggerAction:"all",
				    	mode:"local",
				    	store: new Ext.data.JsonStore({
				    		autoLoad: false,
				    		root: 'data',
				    		fields: [
				    		         'code',
				    		         'name'
				    		],
				    		url:"/json/okato/"
				    	}),
				    	listeners : {
				    		select: function(combo, record, index){
				    			if(record.data.code)
				    				Ext.getCmp('okato_code_filed').setValue(record.data.code);
				    		}
				    	}
				    }
				    */
				]
         });
         
         Ext.ux.Okato.superclass.initComponent.apply(this, arguments);
	 },
	 
	 addComboPartition: function(){
		 
	 }
	
});

Ext.reg('okato', Ext.ux.Okato);
