/*
 * Выбор значения для КЛАДР
 */
Ext.ux.Kladr = Ext.extend( Ext.Panel, {
	
	layout: 'form',
	border: false,
	frame: false,
	
	initComponent: function()
	{
         Ext.apply(this, 
         {
			 defaults:{
				anchor: '100%'
			 },
        	 items: [ 
        	        {
        	        	xtype: 'hidden',
        	        	id: 'infr_address_kladr',
        	        	name: 'infr_address_kladr'
        	        },
				    {
				    	xtype: 'combo',
				    	id: 'kladr_province_combo',
				    	fieldLabel:"Область",
				    	displayField: "name",
				    	valueField:"code",
				    	typeAhead: true,
				    	lazyRender: true,
				    	triggerAction:"all",
				    	mode:"local",
				    	store: new Ext.data.ArrayStore({
				    		autoLoad: true,
				    		fields: ['code', 'name'],
				    		url:"/home/infr/kladr/"
				    	}),
				    	listeners : {
				    		select: function(combo, record, index){
				    			/*
				    			 * Сбрасываем данные на случай неудачной загрузки хранилищ
				    			 */
				    			var kladr_district= Ext.getCmp('kladr_district').getStore();
				    				kladr_district.removeAll();
				    				
				    			var kladr_city = Ext.getCmp('kladr_city').getStore();
				    				kladr_city.removeAll();
				    				
				    			var kladr_locality = Ext.getCmp('kladr_locality').getStore();
				    				kladr_locality.removeAll();
				    				
				    			var kladr_street = Ext.getCmp('kladr_street').getStore();
				    				kladr_street.removeAll();
				    				
				    			kladr_district.setBaseParam('code', record.data.code);
				    			kladr_district.load();
				    			
				    			kladr_city.setBaseParam('code', record.data.code);
				    			kladr_city.load();
				    			
				    			if(record.data.code)
				    				Ext.getCmp('infr_address_kladr').setValue(record.data.code);
				    		}
				    	}
				    },
				    {
				    	xtype: 'combo',
				    	id: 'kladr_district',
				    	fieldLabel:"Район",
				    	displayField: "name",
				    	valueField:"code",
				    	valueField:"code",
				    	typeAhead: true,
				    	lazyRender: true,
				    	triggerAction:"all",
				    	mode:"local",
				    	store: new Ext.data.ArrayStore({
				    		autoLoad: false,
				    		fields: ['code', 'name'],
				    		url:"/home/infr/kladr/2/"
				    	}),
				    	listeners : {
				    		select: function(combo, record, index){
				    			/*
				    			 * Сбрасываем данные на случай неудачной загрузки хранилищ
				    			 */				    				
				    			var kladr_city = Ext.getCmp('kladr_city').getStore();
				    				kladr_city.removeAll();
				    				
				    			var kladr_locality = Ext.getCmp('kladr_locality').getStore();
				    				kladr_locality.removeAll();
				    				
				    			var kladr_street = Ext.getCmp('kladr_street').getStore();
				    				kladr_street.removeAll();
				    				
				    				kladr_city.setBaseParam('code', record.data.code);
				    				kladr_city.load();
				    				
				    				kladr_locality.setBaseParam('code', record.data.code);
				    				kladr_locality.load();
				    				
				    				if(record.data.code)
				    					Ext.getCmp('infr_address_kladr').setValue(record.data.code);
				    		}
				    	}
				    },
				    {
				    	xtype: 'combo',
				    	id: 'kladr_city',
				    	fieldLabel:"Город",
				    	displayField: "name",
				    	valueField:"code",
				    	typeAhead: true,
				    	lazyRender: true,
				    	triggerAction:"all",
				    	mode:"local",
				    	store: new Ext.data.ArrayStore({
				    		autoLoad: false,
				    		fields: ['code', 'name'],
				    		url:"/home/infr/kladr/3/"
				    	}),
				    	listeners : {
				    		select: function(combo, record, index){
				    			/*
				    			 * Сбрасываем данные на случай неудачной загрузки хранилищ
				    			 */				    				
				    			var kladr_locality = Ext.getCmp('kladr_locality').getStore();
				    				kladr_locality.removeAll();
				    				
				    			var kladr_street = Ext.getCmp('kladr_street').getStore();
				    				kladr_street.removeAll();
				    				
				    			kladr_locality.setBaseParam('code', record.data.code);
				    			kladr_locality.load();
				    			
				    			kladr_street.setBaseParam('code', record.data.code);
				    			kladr_street.load();
				    			
				    			if(record.data.code)
				    				Ext.getCmp('infr_address_kladr').setValue(record.data.code);
				    		}
				    	}
				    },
				    {
				    	xtype: 'combo',
				    	id: 'kladr_locality',
				    	fieldLabel:"Населенный пункт",
				    	displayField: "name",
				    	valueField:"code",
				    	typeAhead: true,
				    	lazyRender: true,
				    	triggerAction:"all",
				    	mode:"local",
				    	store: new Ext.data.ArrayStore({
				    		autoLoad: false,
				    		fields: ['code', 'name'],
				    		url:"/home/infr/kladr/4/"
				    	}),
				    	listeners : {
				    		select: function(combo, record, index){
				    			/*
				    			 * Сбрасываем данные на случай неудачной загрузки хранилищ
				    			 */				    				
				    			var kladr_street = Ext.getCmp('kladr_street').getStore();
				    				kladr_street.removeAll();
				    				
				    				kladr_street.setBaseParam('code', record.data.code);
				    				kladr_street.load();
				    				
				    				if(record.data.code)
				    					Ext.getCmp('infr_address_kladr').setValue(record.data.code);
				    		}
				    	}
				    },
				    {
				    	xtype: 'combo',
				    	id: 'kladr_street',
				    	fieldLabel:"Улица",
				    	displayField: "name",
				    	valueField:"code",
				    	typeAhead: true,
				    	lazyRender: true,
				    	triggerAction:"all",
				    	mode:"local",
				    	store: new Ext.data.ArrayStore({
				    		autoLoad: false,
				    		fields: ['code', 'name'],
				    		url:"/home/infr/kladr/5/"
				    	}),
				    	listeners : {
				    		select: function(combo, record, index){
				    			if(record.data.code)
				    				Ext.getCmp('infr_address_kladr').setValue(record.data.code);
				    		}
				    	}
				    }
				]
         });
         
         Ext.ux.Kladr.superclass.initComponent.apply(this, arguments);
	}
	
});

Ext.reg('kladr', Ext.ux.Kladr);
