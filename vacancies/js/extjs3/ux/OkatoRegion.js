/*
 * Выбор значения для ОКАТО
 */
var okatoStore = new Ext.data.JsonStore({
	autoLoad: true,
	root: 'data',
	fields: [
	         'code',
	         'name'
	],
	url:"/json/okato/region/"
});

Ext.ux.Okato = Ext.extend( Ext.Panel, {
	
	layout: 'form',
	border: false,
	frame: false,
	
	initComponent: function()
	{
         Ext.apply(this, 
         {
			 defaults:{
				anchor: '100%'
			 },
        	 items: [ 
				    {
				    	xtype: 'combo',
				    	name: 'okato_code',
				    	hiddenName: 'okato_code',
				    	id: 'okato_code',
				    	fieldLabel:"Муниципальное образование",
				    	displayField: "name",
				    	valueField:"code",
				    	typeAhead: true,
				    	lazyRender: true,
				    	triggerAction:"all",
				    	mode:"local",
				    	store: okatoStore
				    }
				]
         });
         
         Ext.ux.Okato.superclass.initComponent.apply(this, arguments);
	 },
	 
	 addComboPartition: function(){
		 
	 }
	
});

Ext.reg('okato', Ext.ux.Okato);
