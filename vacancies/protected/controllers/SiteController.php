<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Full list of languages for combobox
	 */
	public function actionLanguageList()
	{
		$data = array(array('id' => -1, 'title' => 'All'));
		$rows = DictionaryLanguages::model()->findAll();
		foreach ($rows as $row) {
			$data[] = array('code' => $row->code, 'language' => $row->language);
		}

		SiteController::returnJsonData($data);
	}

	/**
	 * Full list of departments for combobox
	 */
	public function actionDepartmentList()
	{
		$data = array(array('id' => -1, 'title' => 'All'));
		$rows = Departments::model()->findAll();
		foreach ($rows as $row) {
			$data[] = array('id' => $row->id, 'title' => $row->title);
		}

		SiteController::returnJsonData($data);
	}

	/**
	 * Vacancies list according to current selection
	 */
	public function actionVacancyList()
	{
		$departmentId = Yii::app()->request->getParam('departmentId');
		$languageCode = Yii::app()->request->getParam('languageCode');

		$model = Vacancies::model();
		$arrayWith = array(
			'department'=> array('title'));
		$findCriteria = new CDbCriteria();
		if (!is_null($departmentId) && $departmentId > 0) {
			$findCriteria->addCondition("department_id = $departmentId");
		}

//		if (!is_null($languageCode) && is_string($languageCode)) {
//			$findCriteria->addCondition("language_code = '$languageCode'");
//			$arrayWith['vacantTranslations'] = array('vacant_name', 'vacant_description');
//		}

		$data = array();
		$rows = $model->with($arrayWith)->findAll($findCriteria);
		foreach ($rows as $row) {
			$data[] = array(
				'vacancy' => $row->title,
				'description' => $row->description,
				'department' => $row->department->title
					);
		}

		SiteController::returnJsonData($data);
	}
}