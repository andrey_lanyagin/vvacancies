<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public static function returnJSON($data) {
		header("content-type: application/json");
		echo json_encode($data);
	}

	public static function returnSuccess() {
		header("content-type: application/json");
		echo json_encode(array("success" => true));
	}

	public static function returnFailure($err) {
		header("content-type: application/json");
		echo json_encode(array("success" => false, "message" => $err['message']));
	}

	/**
     * Json data response for JsonStore
     * @param array $data
     * @param array $params - OPTIONAL Other params to send
     */
    static public function returnJsonData(array $data , $params = false, $total = 0 ){
    	$result = array(
    		'success'=>true,
    		'data'=>&$data,
            'total'=>$total
    	);
    	if(!empty($params)){
    		foreach($params as $k=>$v){
    			$result[$k] = $v;
    		}
    	}

    	echo json_encode($result);
    }
}